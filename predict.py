import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

plt.style.use("ggplot")

import torch
import torch.nn as nn
import torchvision.transforms as transforms

import timm

import gc
import os
import time
import random
from datetime import datetime

from PIL import Image
from tqdm.notebook import tqdm
from sklearn import model_selection, metrics

# general global variables
DATA_PATH = "input/"
MODEL_PATH = "input/"
MODEL_NAME = "model_5e_202104070819.pth"
# model specific global variables
IMG_SIZE = 224
BATCH_SIZE = 16

# df = pd.read_csv(os.path.join(DATA_PATH, "train.csv"))
# train_df, valid_df = model_selection.train_test_split(
#     df, test_size=0.1, random_state=42, stratify=df.label.values
# )

class ViTBase16(nn.Module):
    def __init__(self, n_classes, pretrained=False):

        super(ViTBase16, self).__init__()

        self.model = timm.create_model("vit_base_patch16_224", pretrained=False)
        if pretrained:
            self.model.load_state_dict(torch.load(MODEL_PATH))

        self.model.head = nn.Linear(self.model.head.in_features, n_classes)

    def forward(self, x):
        x = self.model(x)
        return x

transforms_valid = transforms.Compose(
    [
        transforms.Resize((IMG_SIZE, IMG_SIZE)),
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ]
)

def predict(path, img_name):
    device  = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    base_path = path 
    # img_name = '2216849948.jpg'
    img_path = os.path.join(base_path,img_name)
    print(img_path)
    image = Image.open(img_path).convert("RGB")
    image = transforms_valid(image)
    image = image.to(device)
    image = torch.unsqueeze(image,0)
    model = ViTBase16(n_classes=5, pretrained=False)
    model.load_state_dict(torch.load(os.path.join(MODEL_PATH,MODEL_NAME)))
    model = model.to(device)
    model.eval()
    with torch.no_grad():
        y_preds = model(image)
        print("label of image " + img_name + " is :")
        print(y_preds.argmax(1).cpu().detach().numpy())
    label = y_preds.argmax(1).cpu().detach().numpy()
    return label[0]