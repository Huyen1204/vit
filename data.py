#0
cbb = { # 0
    'name': 'Cassava Bacterial Blight',
    'subtitles': [
        {
            'subtitle' : 'Introduction',
            'content' : """Xanthomonas axonopodis pv. manihotis is the pathogen that causes bacterial blight of cassava. Originally discovered in Brazil in 1912, the disease has followed cultivation of cassava across the world.[1] Among diseases which afflict cassava worldwide, bacterial blight causes the largest losses in terms of yield.
            """
        },
        {
            'subtitle' : 'Hosts and symptoms',
            'content' : """Xanthomonas axonopodis pv. manihotis is capable of infecting most members of the plant genus Manihot.[1] Consisting of about 100 species, the most economically significant species is easily the widely cultivated woody shrub, Manihot esculenta, known colloquially as the cassava plant.[2] In cassava, symptoms vary in a manner that is unique to this pathogen. Symptoms include blight, wilting, dieback, and vascular necrosis. A more diagnostic symptom visible in cassava with X. axonopodis infection are angular necrotic spotting of the leaves—often with a chlorotic ring encircling the spots. These spots begin as distinguishable moist, brown lesions normally restricted to the bottom of the plant until they enlarge and coalesce—often killing the entire leaf. A further diagnostic symptom often embodies itself as pools of gum exudate along wounds and leaf cross veins. It begins as a sappy golden liquid and hardens to form an amber colored deposit.[1]
            """
        },
        {
            'subtitle' : "Causal agent and disease cycle",
            'content' : """Xanthomonas axonopodis pv. manihotis is a vascular and foliar pathogenic species of bacteria. It normally enters its host plants through stomatal openings or hydathodes. Wounds to stems have also been noted as a means of entry. Once inside its host, X. axonopodis enzymatically dissolves barriers to the plant's vascular system and so begins a systemic infection. Because of its enzymes inability to break down highly lignified cell walls, this pathogen prefers to feed on younger tissues and often follows xylem vessels into developing buds and seeds. Seeds which have been invaded by a high number of bacteria are sometimes deformed and necrotic, but assays have shown a high percentage of infected seeds are asymptomatic carriers. In moist conditions, Xanthomonas axonopodis pv. manihotis has been shown to survive asymptomatically for up to thirty months without new host tissue, but is a poor survivor in soil. It persists from one growing season to the next in infected seeds and infected clippings planted as clones in fields. Once one cassava plant is infected, the whole crop is put at risk to infection by rainsplash, contaminated cultivation tools, and foot traffic.[1][3][4] These are effective methods of transmission because they cause wounds to healthy cassava plants, and X. axonopodis uses these wounds as an entry point.

            """
        },
        {
            'subtitle' : "Environment",
            'content' : """Hailing from Brazil, Xanthomonas axonopodis pv. manihotis excels in a humid subtropical to tropical climate. Plantations of Manihot esculenta often take place in soils characterized for being arid, lacking in nutrients, and prone to erosion especially when the cultivation occurs in sloped fields.[5] This crop is commonly found in the tropical and subtropical regions of Africa, Asia and Latin America, and X. axonopodis has followed it. It has been confirmed up and down Latin America into North America, Sub-Saharan Africa, Southeast Asia/India, and even Polynesia. These regions are prone to high precipitation rates, a climate variable that propagates Xam epidemics.[6] Constant rain events and windsplashes are the main form transmission of the secondary inoculum of the disease. Nevertheless, the high humidity and high air temperatures (77–90 °F) of tropical and subtropical regions make Cbb a conducive epidemic.[7] The favorable conditions described allow colonial growth and eventual swarm behavior to enter hydathodes, stomata, or wounds.
            """
        },
        {
            'subtitle': "Pathogenesis",
            'content': """Similar to other phytobacteria, Xam requires the assembly of a type 3 secretion system (T3SS) to initiate infection.[8] After cell recognition by the T3SS, Xam releases type III effector proteins to modulate the cell's innate immunity. These effectors are denominated as transcription activator-like (TAL) effectors.[9] While varying among strains, TAL effectors maintain several domains conserved. These include an N-terminal type III secretion signal, a central domain, a transcription acidic activation domain (AAD), and a C-terminal nuclear localization signal (NLS).[10] N-terminal type III secretion signal allows the entry of the effector through the T3SS into the cell, and the NLS mobilizes the effector into the cell nucleus. The latter acts by emitting signals and recruiting host proteins for translocation.[11] After gaining nuclear entry, sequence-specific protein-DNA interactions are carried by the central region, which recognizes a specific DNA sequence to which it attaches. The specificity is acquired by a two-residues combination (12 and 13 mer) in every tandem repeat that composes the central region; a change in a residue will result in a change of specificity towards a promoter. Finally, AAD is known as the cause of final transcription modulation, essential for virulence or avirulence.[10][12] It has been recorded that Xam works with the activation of SWEET sugar transporters, promoting the efflux of glucose and sucrose to the apoplasm for bacterial benefit.[11]

                """
        },
        {
            'subtitle' : "Management",
            'content' : """Cultural approaches:

Several disease management techniques have been developed to control Cbb incidence and dissemination. Cultural practices have been used to decrease the incidence of the pathogen or delay the effect of the disease in the field. Pruning or total extirpation of infected plant tissue, weed removal, use of certified seeds, bacterial analysis of stem cuttings and crop rotation are used the most to limit the disease presence in the field.[12] Transplantation of clones is the most common mode of propagation of this crop so the most important control of bacterial wilt of cassava is planting uninfected clones. In areas where bacterial wilt has not yet been established, it is important to raise a new crop from a meristem culture certifiably free of disease. In areas where the disease is already prevalent, great care should be taken to ensure clippings are taken from healthy plants and even then from the highly lignified portion at the base of cassava plants which appear healthy.[4]

Seeds are known to be able harbor the pathogen, but successful sanitation measures have been described. Infected seed immersed in water at 60 °C showed no sign of bacterial survival while the seed showed no reduction in germination potential.[3] Furthermore, the sanitation of tools and big machinery are crucial to avoid the infection of healthy plants through mechanical inoculation.

Intercropping and crop rotation have both been implemented in cassava cultivation and have been successful.[13] In the instance of crop rotation following an infected cassava crop, deep soil turnover is recommended and a period of six months should be observed before cassava is planted again; X. axonopodis is a poor soil survivor and does not sporulate so this time frame should clear crop fields of inoculum. It is also important to clear the field of weeds as X. axonopodis is known to survive much longer epiphytically on weeds than it does in soil.[1]
Biological control:

Colombian clones of cassava normally susceptible to bacterial blight showed a yield increase by a factor of 2.7 when applications of Pseudomonas fluorescens and P. putida were applied four times a month during the growing season. This approach shows promise but requires further investigation.[1][3]
Host resistance:

The last commonly used method is the mixed cultures using resistant cultivars, keeping in mind that yield can be affected with certain cultivars.[14] There is considerable variance in cassava resistance to bacterial wilt, and this is a promising means of control. The resistance which has been identified in South American strains of cassava works by preventing colonization of the xylem. The genes which control this resistance are currently being mapped and implementation efficiency should see uptick in coming years.[15]

            """
        },
        {
            'subtitle' : "Importance",
            'content' : """Cassava is a staple of the human diets in developing countries in the tropics. 2007 global production was 228 million tons, with 52% coming from Africa. It is estimated that cassava accounts for 37% of total calories consumed by humans in Africa.[16] It has been further estimated that it provides the sixth most calories of any crop worldwide.[13] These numbers would probably be even more impressive if bacterial blight were eradicated. Estimates for how much cassava crop Xanathomas axonopodis pv. manihotis destroys every year vary widely but studies have shown that one infected transplant can end in 30% loss of yield in one growing cycle, and up to 80% by the third cycle if no control measures are taken.[1] There have been a number of historical outbreaks of bacterial blight. Zaire lost 75% of its tuber yield and almost all of its protein-rich leaf yield every year of the early 1970s, while parts of Brazil lost 50% of tuber yield in 1974.[1]
            """
        }
    ]
}
#1
cbsd = { # 1
    'name': 'Cassava Brown Streak Disease',
    'subtitles': [
        {
            'subtitle' : 'Introduction',
            'content' : """Cassava brown streak virus disease (CBSD) is a damaging disease of cassava plants, and is especially troublesome in East Africa.[1] It was first identified in 1936 in Tanzania, and has spread to other coastal areas of East Africa, from Kenya to Mozambique. Recently, it was found that two distinct viruses are responsible for the disease: cassava brown streak virus (CBSV) and Ugandan cassava brown streak virus (UCBSV). Both have (+)ss RNA genomes, belong to the genus Ipomovirus in the family Potyviridae, and produce generally similar symptoms in infected plants.[2][3][4] Root rot renders the cassava tuber inedible, resulting in severe loss of economic value; therefore, current research focuses on achieving cultivars that do not develop the necrotic rot.[4] This disease is considered to be the biggest threat to food security in coastal East Africa and around the eastern lakes.
            """
        },
        {
            'subtitle' : 'Symptoms',
            'content' : """CBSD is characterized by severe chlorosis and necrosis on infected leaves, giving them a yellowish, mottled appearance.[3] Chlorosis may be associated with the veins, spanning from the mid vein, secondary and tertiary veins, or rather in blotches unconnected to veins. Leaf symptoms vary greatly depending on a variety of factors. The growing conditions (i.e. altitude, rainfall quantity), plant age, and the virus species account for these differences.[5] Brown streaks may appear on the stems of the cassava plant. Also, a dry brown-black necrotic rot of the cassava tuber exists, which may progress from a small lesion to the whole root. Finally, the roots can become constricted due to the tuber rot, stunting growth.[6] Typically the affected plants do not possess all of these characteristics, but those that are severely affected may. Farmers may be unaware of their infected cassava crops until they are harvested and see the tuber lesions because leaves are asymptomatic.[1] The cassava mosaic virus (CMV) is another cassava virus that exhibits foliar symptoms similar to CBSD, but they are more obvious.

UCBSV has milder symptoms than CBSV, and lower pathogenicity.[2][7] """
        },
        {
            'subtitle' : "Vector and disease cycle",
            'content' : """After a period of ambiguity among researchers, the consensus is that the most likely candidate of CBSD vector is Bemisia tabaci biotype B, the silverleaf whitefly.[1][8][9] It is also sometimes referred to as Bemisia argentifolii.[10] There is a close association between surging whitefly populations and CBSD incidence.[11] This whitefly species is also considered to be the vector of CMV. It is suggested, however, that B. tabaci whiteflies transmit CBSVs less effectively than CMVs.[4] Also, the CBSD retention period in B. tabaci may not exceed more than 24 h, but more research is needed to confirm this.[4]

The adult B. tabaci lives an average of sixteen days, and the maturation process from egg to adult is thirty days.[12] Eggs may be deposited haphazardly or in a spiral fashion on the leaf undersides. Both juvenile and adult whiteflies feed on the phloem of the leaves by inserting a sucker mouth part into the leaf, thereby transmitting the virus to the plant. Saliva containing toxins is also injected into the cassava plant while whiteflies feed, disturbing plant growth and ultimately reducing yield. Seedlings are particularly affected.[13]
            """
        },
        {
            'subtitle' : "Disease Spread",
            'content' : """After its first identification in 1936, CBSD was almost totally eliminated in Uganda due to program efforts, and there were relatively small yield losses in affected areas. The disease was restricted to < 1000 m above sea level along coastal Kenya to Mozambique, and the shores of Lake Malawi. However, as of the year 2000, CBSD has spread rapidly throughout Eastern Africa.[14] Midaltitude levels (1200–1500 m above sea level) now accommodates CBSD, as it has been reported in Uganda, Democratic Republic Congo, and around Tanzanian lake zones.[5] As of 2009, CBSD outbreaks were most prevalent in south - central Uganda and in Mara Region. There have also been reports of CBSD in Rwanda and Burundi.[4]

The incidence of CBSD is greatest in Uganda where there is resistance to CMV in cassava and other locations in general.[1][2] Recent surveys demonstrated that of the 23 districts in Uganda surveyed, 70% had CMD-resistant cassava varieties, and all are vulnerable to CBSD – causing viruses. These varieties also hosted as many as 200 adult whiteflies on the top five cassava leaves.[5]

Predictions cannot be made about the spreading pattern. Because the disease does not fan out from only one source point, but rather appears in remote areas, or “hot spots,” models are difficult to devise. This challenge may arise from the movement of cuttings from infected regions and/or an abundance of whiteflies in a particular area.[4] 
            """
        },
        {
            'subtitle': "Management of CBSD",
            'content': """Management tools are still being explored for the control of CBSD, and progress has been slow.[11] The development of cassava with resistance to both CMD and CBSD is needed.[5] In a few cassava varieties, natural resistance has been found against UBCSV.[2] Widespread distribution of germplasm of these varieties can reduce disease incidence on a large scale. Furthermore, screening for resistance in farmer-preferred cassava genotypes in Africa is crucial for effective CBSD control and management.[7]

Genetic Engineering

Genetic engineering specific to the RNA genome is used to encourage resistance in cassava cultivars. A recent study demonstrated that inducing the expression of hairpin RNA homologous to viral sequences is a potentially effective lab technique because it imitates the behavior of the plant immune system encountering foreign bodies. Specifically, they were able to use hairpin RNA homologous to the 3’ end of CBSV coat protein sequences in the cassava cultivar 60444 to develop resistance to both CBSV and UCBSV. The resulting construct was transferred to a cultivar that farmers prefer (Nigerian landrace TME 7). This particular cultivar exhibited CMV resistance originally in its natural state, the motive being to foster resistance to both CMV and CBSV post – grafting, which was successful. Therefore, the suggestion is that exploiting the immune system of plants that already have natural resistance to CMV is a potentially viable method to combat both viruses.[2]

Education

Farmers need to be better educated on the subject of CBSD, including cause, diagnosis and disease spread. The most obvious symptom of the disease is the cassava root rot, and farmers are inclined to believe that too much water causes the rotting rather than the virus. The identification of the foliar symptoms is important, because farmers can get a more accurate yield expectation without waiting for the harvest period. Also, awareness of tolerant varieties can be promoted.[11] It is suggested that workshops be held for researchers, so that they are aware of new diagnostics.[6]

Need for More Data

More surveys need to be conducted so that the disease spread and variant affinity can be better understood. Discouragement of affected varieties as crops can be more quickly done with tracking.[1] There is emphasis on the need for more research pertaining to the viral pathogenesis.

Other Suggestions

    Slash-and-burn

In certain areas, destroying cassava plants with CBSD through slash-and-burn techniques and replacing them with resistant or more tolerant strains is recommended

    Border surveillance

Tightening surveillance points between countries (i.e. Tanzania - Uganda and Kenya borders)

    Regulatory testing of tissue samples cross-border

Only allowing the transport of tested germplasm cross-border [1] 
                """
        },
        {
            'subtitle' : "Economic Importance",
            'content' : """Cassava is a very important staple crop for many in Africa, and the demand for it increases with high population growth rates.[4] CBSD poses a serious threat to farmers in East Africa, because crop yields can be reduced as drastically as 70%.[14] Upon harvesting, farmers will cut out the necrotic lesions of affected tubers or they will discard tubers that are severely affected. 10 – 30% of root rot constitutes moderate infection, decreasing the market value of tubers by 90%. It is estimated that African farmers collectively lose revenue of up to $100 million annually due to the devastating disease.[5]
            """
        }
    ]
}
#2
cgm = { # 2
    'name': 'Cassava Green Mottle',
    'subtitles': [
        {
            'subtitle' : 'Introduction',
            'content' : """
                       Cassava green mottle nepovirus. It has not been confirmed to be a nepovirus; these are viruses that are transmitted by nematodes.
                       Distribution is narrow. Only known from Solomon Islands. It was first found on Choiseul in the 1970s; more recently (2010), similar symptoms were seen on Malaita. 
                        """
        },
        {
            'subtitle' : 'Hosts',
            'content' : """It is only known from cassava, and only from Choiseul (and possibly Malaita), nowhere else in the world. In the lab, several plant species have been infected with this virus; these species are known as "indicator" plants and are used to identify and characterise many kinds of viruses: Chenopodium, Nicotiana (tobacco), Phaseolus (French bean), Cucumis (cucumber), Ricinus (castor oil) and Solanum (potato). Interestingly, Ipomoea (sweetpotato) was also infected in these tests. But whether or not infection occurs in the field is unknown.
            """
        },

        {
            'subtitle' : 'Symptoms & Life Cycle',
            'content' : """
            Young leaves are puckered with faint to distinct yellow spots (Photo 1), green patterns (mosaics), and twisted margins (Photo 2). Usually, the shoots recover from symptoms and appear healthy. Occasionally, plants become severely stunted, edible roots are absent or, if present, they are small and woody when cooked.

In the lab, the virus can be passed between plants in sap, and also in seed. Thirty percent of the seed of infected tobacco plants was infected. Whether it also spreads in seed of cassava is unknown. Seed is not used for growing cassava, so spread on Choiseul is most likely in diseased cuttings. However, there are other possibilities.

In a trial on Choiseul, 4% of plants grown from cuttings taken from Guadalcanal became diseased within 250 days from planting. How this infection occurred is unknown, but there are several possibilities. Plants touching each other in the wind may have transferred sap between those infected and healthy. There is also the possibility of spread by nematodes and pollen. The virus is thought to be a member of the nepovirus group, and members of this group are spread in these ways. 
            
            """
        },

        {
            'subtitle' : "Impact",
            'content' : """Surveys on Chosieul showed that the disease is present in most plantings, but the number of infected plants is low. Cuttings taken from diseased plants are much slower to develop than those from plants without symptoms during the previous 9 months, and assumed to be healthy. There were differences between the heights of these two groups of plants at 4 months, but differences were much less at harvest at 8 months. Also, weight of stems and edible roots of diseased plants were half those planted with healthy cuttings. However, farmers usually plant three cuttings per mound, so any that stay healthy have more space and nutrients and make up for the loss in yield from those that are diseased.
            """
        },
        {
            'subtitle' : "Detection & Inspection",
            'content' : """Look for yellow patterns on the leaves, from small dots to irregular patches of yellow and green. Look for leaf margins that are distorted. The plants may be stunted.
            """
        },
        {
            'subtitle': "Management",
            'content': """QUARANTINE
Surveys carried out in Solomon Islands in the late 1970s failed to find the disease anywhere but on the island of Choiseul. It was not found in the nearby Shortland Islands. People on Choiseul were made aware of the disease and asked not to take cuttings to other islands. However, there are no laws restricting the movement of planting material between islands of the country, so compliance is voluntary. Choiseul is near the island of Bougainville in Papua New Guinea, and residents of both islands move between the two, but it is not known if the virus disease occurs in Bougainville.

CULTURAL CONTROL
Cultural control is very important, and is the main method of reducing the impact of this disease:

Before planting:

    Take cuttings for propagation only from plants that are free from symptoms and which, at harvest, produced high yields of edible roots.

During growth

    Remove plants with symptoms as soon as they are seen, and burn them. DO NOT wait until harvest, as by then the plants may have recovered from symptoms and, unless marked, difficult to identify as infected.

RESISTANT VARIETIES
None are known.

CHEMICAL CONTROL
Chemical control is not appropriate for this disease, especially as the method of spread is unknown. 
                """
        }
    ]
}
#3
cmd = { # 3
    'name': 'Cassava Mosaic Disease',
    'subtitles': [
        {
            'subtitle' : 'Introduction',
            'content' : """Cassava mosaic virus is the common name used to refer to any of eleven different species of plant pathogenic virus in the genus Begomovirus. African cassava mosaic virus (ACMV), East African cassava mosaic virus (EACMV), and South African cassava mosaic virus (SACMV) are distinct species of circular single-stranded DNA viruses which are transmitted by whiteflies and primarily infect cassava plants; these have thus far only been reported from Africa. Related species of viruses (Indian cassava mosaic virus, ICMV) are found in India and neighbouring islands (Sri Lankan cassava mosaic virus, SLCMV), though cassava is cultivated in Latin America as well as Southeast Asia. Nine species of cassava-infecting geminiviruses have been identified between Africa and India based on genomic sequencing and phylogenetic analysis. This number is likely to grow due to a high rate of natural transformation associated with CMV.[1]

The viruses are members of the family Geminiviridae and the genus Begomovirus. The first report of cassava mosaic disease (CMD) was from East Africa in 1894.[2] Since then, epidemics have occurred throughout the African continent, resulting in great economic loss and devastating famine.[2] In 1971, a resistant line of cassava, the predominant host of the pathogen, was established and used by the International Institute of Tropical Agriculture in Nigeria. This resistance worked as an effective control for many years. However, in the late 20th century, a more virulent virus broke out in Uganda and quickly spread to East and Central Africa.[2] This highly virulent strain was later discovered to be a chimaera of two distinct Begomovirus species.[1]

CMD is primarily managed through phytosanitation practices as well as the use of conventional resistance breeding. Additionally, vector management and cross-protection help to minimize transmission and symptom development.[2] Though management practices are useful, the viruses’ high rate of recombination and co-infection capabilities have caused CMD to be one of the most detrimental diseases affecting food supply in Africa.[1]
            """
        },
        {
            'subtitle' : 'Hosts and symptoms',
            'content' : """Cassava originated in South America and was introduced to Africa in relatively recent times.[2] It is known to be a very drought-tolerant crop with the ability to yield even when planted in poor soils. When cassava was first grown in Africa, it was used for subsidiary purposes though it is now considered to be one of the most important food staple crops on the continent.[2] Its production is moving toward an industrialized system in which plant material is used for a variety of products including starch, flour, and animal feed.[3]

As cassava is vegetatively propagated, it is particularly vulnerable to viruses and thus Cassava geminiviruses lead to great economic loss each year.[1] When these infect a host plant, the plant’s defense system is triggered. Plants use gene silencing to suppress viral replication, though begomoviruses have evolved a counter-acting suppressor protein against this natural host defense.[1] Because different species of Begomovirus produce different variants of this suppressor protein, co-infection by multiple species typically leads to more severe disease symptoms.[4]

Initially following infection of a cassava geminivirus in cassava, systemic symptoms develop.[1] These symptoms include chlorotic mosaic of the leaves, leaf distortion, and stunted growth.[5] Leaf stalks have a characteristic S-shape.[6] Infection can be overcome by the plant especially when a rapid onset of symptoms occurs. A slow onset of disease development usually correlates with death of the plant.[1]

Though the cassava-infecting geminiviruses causes most of their economic damage in cassava, they are able to infect other plants. The host range depends on the species of virus and most are able to be transmitted and to cause disease on plants of the genera Nicotiana and Datura.[7]

Cassava Mosaic Disease is currently spreading across SE Asia.[8]"""
        },
        {
            'subtitle' : "Causal agent and disease cycle",
            'content' : """Cassava geminiviruses[9] are transmitted in a persistent manner by the whitefly Bemisia tabaci, by vegetative propagation using cuttings from infected plants, and occasionally by mechanical means.[10][11][9] Cassava produces its first leaves within 2–3 weeks of planting; these young leaves are then colonized by the viruliferious whiteflies.[12] This is the key infection period for CMD geminiviruses, as they cannot infect older plants.[13] As the genome of the viruses has two components, DNA A and B, that are encapsidated in separate geminate particles, it requires a double inoculation to cause infection.[9]

Generally, whitefly requires 3 hours feeding time to acquire the virus, a latent period of 8 hours, after which it needs 10 minutes to infect the young leaves.[13] There is variation in the literature on this score, however, with other sources citing a 4-hour acquisition time and 4-hour latent period.[11] Symptoms appear after a 3-5 week latent period.[12] Adult whiteflies can continue to infect healthy plants 48 hours after initial acquisition of the virus.[11] A single whitefly is sufficient to infect the host; however, successful transmission increases when multiple infected whiteflies feed on the plant.[11]

After entering the plant through the leaves, the virus remains in the leaf cells for 8 days.[11] As it is a single-stranded DNA virus, it needs to enter the nucleus of the leaf cells to replicate.[13] After this initial period, the virus enters the phloem and travels to the base of the stem and out into the branches.[11] Travel to the branches of the plant is much slower than travel through the stem, so cuttings of branches from infected stems may be free of disease.[11] Some literature has indicated that infection is limited to above-ground tissue, but it is not clear why this would be the case.[14]

            """
        },
        {
            'subtitle' : "Environment",
            'content' : """The severity of cassava mosaic disease is impacted by environmental factors such as light intensity, wind, rainfall, plant density and temperature. Given that the viruses are transmitted by whitefly, the spread of the virus is going to depend largely on the vector. Temperature is the most important environmental factor controlling the size of the vector population.[13] In the literature, vector-preferred temperature estimates vary from 20 °C to 30 °C[12] to 27 °C to 32 °C[11] but generally high temperatures associated with high fecundity, rapid development, and greater longevity in whitefly.[12] Increased light intensity has been shown to increase activity of the whitefly vector.[11]

Whiteflies can fly at speeds up to 0.2 mph, and in high-wind conditions they can move much greater distances in a shorter time, thus increasing rate of virus spread.[13] This wind-dependent spread is reflected in the location of the whitefly in cassava fields, with populations greatest in upwind borders and lowest within the field.[13]

Virus incidence increases when cassava is growing vigorously.[12] Thus, plant density impacts the spread of the virus, with low-density fields encouraging faster disease propagation than high-density ones.[13] In dry areas, rainfall can be a limiting factor for cassava growth so higher rainfall will be associated with higher incidence of disease.[12] Populations of whitefly will increase with rainfall, but heavy rains may impede whitefly spread and thus decrease incidence of virus.[12]

Timing of planting can play an important role in the severity of disease, with cassava planted in March showing a 74% incidence rate of CMV, compared with 4% in August.[13] Seasonal distribution of the virus will vary with the climate. In tropical rain forest type climates, where it is wet and humid most of the year, rapid virus distribution occurred from November to June, and slow progress occurred from July to September.[12] This timing correlated with higher and lower temperatures. In a study of the disease in the Ivory Coast of Africa, maximum rate of disease spread was reached two months after planting.[11] Little to no infection occurs after three months, and variation in spread was due to change in temperature, radiation and population levels of whitefly.

            """
        },
        {
            'subtitle': "Control strategies",
            'content': """Control strategies for cassava mosaic disease include sanitation and plant resistance. In this case, sanitation means using cuttings from healthy plants to start with a healthy plot and maintaining that healthy plot by identifying unhealthy plants and immediately removing them. This strategy does not protect them from being inoculated by whiteflies, but research shows that the virus is more aggressive in plants infected from contaminated cuttings than by insect vectors. There are also specific varieties that fare better against some viruses than others, so plant resistance is possible.[12] For example, hybrids that are a result of crossing cassava and other species, such as Manihot melanobasis and M. glaziovii, have been shown to have considerable resistance to CMV.[15]

Prevention methods of CMV spread include, avoiding planting alternative hosts of the virus, such as castor bean (Ricinus communis), adjacent to cassava, avoiding planting cassava if neighbouring fields have virus-infected cassava as these could be carried by Whiteflies. Prevention methods also include not planting alternative hosts of the virus vector Whitefly e.g. tomato.[6]

The CABI-led programme, Plantwise suggests intercropping with cereals and legumes, such as maize or cowpeas, to repel whiteflies and rotate cassava with non-host crops including sorghum.[6]

The Ministry of Agriculture Food Security and Cooperatives of Tanzania recommend uprooting diseased plants once every week by pulling them out by hand. Plants should be carried away from the field and exposed to the sunlight for drying and then burned to kill the viruses.[16]

                """
        },
        {
            'subtitle' : "Importance",
            'content' : """Mostly grown as a food source in Africa, cassava is the third largest source of carbohydrates in the world.[12] In recent times, cassava production has turned from subsistence to commercial production.[1]

CMD was first described in 1894 and is now considered one of the most damaging crop viruses in the world.[12][1] Annual economic losses in East and Central Africa are estimated to be between US$1.9 billion and $2.7 billion.[1] Although cassava is also cultivated in Latin America and South East Asia, the geminiviruses infecting it are only found in Africa and the Indian sub-continent. This has been mainly attributed to the inability of B. tabaci to colonize cassava effectively in this part of the world.[1]

The Ministry of Agriculture Food Security and Cooperatives of Tanzania recommend uprooting diseased plants once every week by pulling them out by hand. Plants should be carried away from the field and exposed to the sunlight for drying and then burned to kill the viruses.

            """
        }
    ],
    'info': "CMD is caused by viruses belonging to the genus Begomovirus in the family Geminiviridae." \
            " It is the single most important viral disease of cassava in Zambia and it also occurs in many neighbouring countries," \
            " including Tanzania, Malawi, Democratic Republic of the Congo, Zimbabwe, Mozambique and Angola."
}
#4
healthy = { # 4
    'name': 'Cassava Information',
    'subtitles': [
        {
            'subtitle' : 'Introduction',
            'content' : """
            Manihot esculenta, commonly called cassava (/kəˈsɑːvə/), manioc,[2] or yuca (among numerous regional names) is a woody shrub native to South America of the spurge family, Euphorbiaceae. Although a perennial plant, cassava is extensively cultivated as an annual crop in tropical and subtropical regions for its edible starchy tuberous root, a major source of carbohydrates. Though it is often called yuca in parts of Spanish America and in the United States, it is not related to yucca, a shrub in the family Asparagaceae. Cassava is predominantly consumed in boiled form, but substantial quantities are used to extract cassava starch, called tapioca, which is used for food, animal feed, and industrial purposes. The Brazilian farinha, and the related garri of West Africa, is an edible coarse flour obtained by grating cassava roots, pressing moisture off the obtained grated pulp, and finally drying it (and roasting in the case of farinha).

Cassava is the third-largest source of food carbohydrates in the tropics, after rice and maize.[3][4] Cassava is a major staple food in the developing world, providing a basic diet for over half a billion people.[5] It is one of the most drought-tolerant crops, capable of growing on marginal soils. Nigeria is the world's largest producer of cassava, while Thailand is the largest exporter of cassava starch.

Cassava is classified as either sweet or bitter. Like other roots and tubers, both bitter and sweet varieties of cassava contain antinutritional factors and toxins, with the bitter varieties containing much larger amounts.[6] It must be properly prepared before consumption, as improper preparation of cassava can leave enough residual cyanide to cause acute cyanide intoxication,[7][8] goiters, and even ataxia, partial paralysis, or death. The more toxic varieties of cassava are a fall-back resource (a "food security crop") in times of famine or food insecurity in some places.[7][6] Farmers often prefer the bitter varieties because they deter pests, animals, and thieves.[9] 
            
            """
        },
        {
            'subtitle' : 'Description',
            'content' : """The cassava root is long and tapered, with a firm, homogeneous flesh encased in a detachable rind, about 1 mm thick, rough and brown on the outside. Commercial cultivars can be 5 to 10 centimetres (2 to 4 inches) in diameter at the top, and around 15 to 30 cm (6 to 12 in) long. A woody vascular bundle runs along the root's axis. The flesh can be chalk-white or yellowish. Cassava roots are very rich in starch and contain small amounts of calcium (16 mg/100 g), phosphorus (27 mg/100 g), and vitamin C (20.6 mg/100 g).[10] However, they are poor in protein and other nutrients. In contrast, cassava leaves are a good source of protein (rich in lysine), but deficient in the amino acid methionine and possibly tryptophan.[11]
            """
        },
        {
            'subtitle' : "History",
            'content' : """Wild populations of M. esculenta subspecies flabellifolia, shown to be the progenitor of domesticated cassava, are centered in west-central Brazil, where it was likely first domesticated no more than 10,000 years BP.[12] Forms of the modern domesticated species can also be found growing in the wild in the south of Brazil. By 4,600 BC, manioc (cassava) pollen appears in the Gulf of Mexico lowlands, at the San Andrés archaeological site.[13] The oldest direct evidence of cassava cultivation comes from a 1,400-year-old Maya site, Joya de Cerén, in El Salvador.[14] With its high food potential, it had become a staple food of the native populations of northern South America, southern Mesoamerica, and the Taino people in the Caribbean islands, who grew it using a high-yielding form of shifting agriculture by the time of European contact in 1492.[15] Cassava was a staple food of pre-Columbian peoples in the Americas and is often portrayed in indigenous art. The Moche people often depicted yuca in their ceramics.[16]

Spaniards in their early occupation of Caribbean islands did not want to eat cassava or maize, which they considered insubstantial, dangerous, and not nutritious. They much preferred foods from Spain, specifically wheat bread, olive oil, red wine, and meat, and considered maize and cassava damaging to Europeans.[17] The cultivation and consumption of cassava were nonetheless continued in both Portuguese and Spanish America. Mass production of cassava bread became the first Cuban industry established by the Spanish.[18] Ships departing to Europe from Cuban ports such as Havana, Santiago, Bayamo, and Baracoa carried goods to Spain, but sailors needed to be provisioned for the voyage. The Spanish also needed to replenish their boats with dried meat, water, fruit, and large amounts of cassava bread.[19] Sailors complained that it caused them digestive problems.[20] Tropical Cuban weather was not suitable for wheat planting and cassava would not go stale as quickly as regular bread.

Cassava was introduced to Africa by Portuguese traders from Brazil in the 16th century. Around the same period, it was also introduced to Asia through Columbian Exchange by Portuguese and Spanish traders, planted in their colonies in Goa, Malacca, Eastern Indonesia, Timor and the Philippines. Maize and cassava are now important staple foods, replacing native African crops in places such as Tanzania.[21] Cassava has also become an important crop in Asia. While it is a valued food staple in parts of eastern Indonesia, it is primarily cultivated for starch extraction and bio-fuel production in Thailand, Cambodia or Vietnam.[22] Cassava is sometimes described as the "bread of the tropics"[23] but should not be confused with the tropical and equatorial bread tree (Encephalartos), the breadfruit (Artocarpus altilis) or the African breadfruit (Treculia africana). This description definitely holds in Africa and parts of South America; in Asian countries such as Vietnam fresh cassava barely features in human diets.[24]

There is a legend that cassava was introduced in 1880-1885 C.E. to the South Indian state of Kerala by the King of Travancore, Vishakham Thirunal Maharaja, after a great famine hit the kingdom, as a substitute for rice.[25] However, there are documented cases of cassava cultivation in parts of the state before the time of Vishakham Thirunal Maharaja.[26] Cassava is called kappa or maricheeni in Malayalam. It is also referred to as tapioca in Indian English usage. 
            """
        },
        {
            'subtitle' : "Production",
            'content' : """In 2018, global production of cassava root was 278 million tonnes, with Nigeria as the world's largest producer, having 21% of the world total (table). Other major growers were Thailand and Democratic Republic of the Congo.[27]
Cassava production – 2018
Country 	Production (millions of tonnes)
 Nigeria 	59.5
 Thailand 	31.7
 Democratic Republic of the Congo 	30.0
 Brazil 	17.6
 Indonesia 	16.1
World 	278
Source: FAOSTAT of the United Nations[27]

Cassava is one of the most drought-tolerant crops, can be successfully grown on marginal soils, and gives reasonable yields where many other crops do not grow well. Cassava is well adapted within latitudes 30° north and south of the equator, at elevations between sea level and 2,000 m (7,000 ft) above sea level, in equatorial temperatures, with rainfalls from 50 to 5,000 mm (2 to 200 in) annually, and to poor soils with a pH ranging from acidic to alkaline. These conditions are common in certain parts of Africa and South America.

Cassava is a highly productive crop when considering food calories produced per unit land area per day (250,000 cal/hectare/day, as compared with 156,000 for rice, 110,000 for wheat and 200,000 for maize).[28] 
            """
        },
        {
            'subtitle': "Economic importance",
            'content': """Cassava, yams (Dioscorea spp.), and sweet potatoes (Ipomoea batatas) are important sources of food in the tropics. The cassava plant gives the third-highest yield of carbohydrates per cultivated area among crop plants, after sugarcane and sugar beets.[29] Cassava plays a particularly important role in agriculture in developing countries, especially in sub-Saharan Africa, because it does well on poor soils and with low rainfall, and because it is a perennial that can be harvested as required. Its wide harvesting window allows it to act as a famine reserve and is invaluable in managing labor schedules. It offers flexibility to resource-poor farmers because it serves as either a subsistence or a cash crop.[30]

Worldwide, 800 million people depend on cassava as their primary food staple.[31] No continent depends as much on root and tuber crops in feeding its population as does Africa. In the humid and sub-humid areas of tropical Africa, it is either a primary staple food or a secondary costaple. In Ghana, for example, cassava and yams occupy an important position in the agricultural economy and contribute about 46 percent of the agricultural gross domestic product. Cassava accounts for a daily caloric intake of 30 percent in Ghana and is grown by nearly every farming family. The importance of cassava to many Africans is epitomised in the Ewe (a language spoken in Ghana, Togo and Benin) name for the plant, agbeli, meaning "there is life".

In Tamil Nadu, India, there are many cassava processing factories alongside National Highway 68 between Thalaivasal and Attur. Cassava is widely cultivated and eaten as a staple food in Andhra Pradesh and in Kerala. In Assam it is an important source of carbohydrates especially for natives of hilly areas.

In the subtropical region of southern China, cassava is the fifth-largest crop in terms of production, after rice, sweet potato, sugar cane, and maize. China is also the largest export market for cassava produced in Vietnam and Thailand. Over 60 percent of cassava production in China is concentrated in a single province, Guangxi, averaging over seven million tonnes annually. 
                """
        }
    ]
}