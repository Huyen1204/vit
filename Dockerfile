# set base image (host OS)
FROM python:latest

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .
RUN apt-get update ##[edited]
# install dependencies
RUN pip install -r requirements.txt
RUN apt-get install libgl1-mesa-glx -y

# copy the content of the local src directory to the working directory
COPY . .

# command to run on container start
CMD [ "python", "./main.py" ]
