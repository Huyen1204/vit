# Cấu trúc project

![project](https://gitlab.com/Huyen1204/vit/-/raw/master/static/images/project_vit.png)

# Mô tả các tệp
| Tệp | Mô tả |
| ------ | ------ |
| input | chứa model đã được train |
| static | chứa ảnh, css, js của project, lưu ảnh lá sắn mà client tải lên |
| templates | chứa các tệp html của project |
| convert.py | chuyển video thành các ảnh |
| data.py | Thông tin các loại bệnh |
| docker-compose.yml | cấu hình container của docker |
| Dockerfile | tệp cấu hình images của docker  |
| main.py | chứa các route |
| predict.py | sử dụng model để dự đoán |
| requirements.txt | chứa các thư viện để cài đặt môi trường |

# Luồng hoạt động xử lý ảnh/video

![flow](https://gitlab.com/Huyen1204/vit/-/raw/master/static/images/flow.png)


# Local

env:

1. Python > 3.6

2. Cài đặt virtualenv

### Môi trường ubuntu

```
pip install virtualenv
virtualenv ubuntu
source ubuntu/bin/activate
pip install -r requirements.txt
```

### Môi trường windows
```
pip install virtualenv
virtualenv windows
windows\Scrips\activate
pip install -r requirements.txt
```
Run project
```
cd VIT
python main.py

```

# Deploy docker
Yêu cầu: docker và docker-compose
Thực thi:
```
./run.sh
```
