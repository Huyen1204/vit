import concurrent.futures
import flask
from flask import Flask, request, render_template
from werkzeug.utils import secure_filename
import os, fnmatch
from predict import predict
from convert import convert
from concurrent.futures import ThreadPoolExecutor
import statistics
import data

app = flask.Flask(__name__, static_url_path="/static")
app.static_folder = 'static'

@app.route("/")
def main():
    return flask.redirect("/cassacare")
    # return flask.render_template("index.html")

@app.route("/cassacare")
def cassacare():
    return flask.render_template("cassacare/index.html")

@app.route("/upload_image", methods=['POST'])
def upload_image():
    f = request.files['file']
    if f.filename == '':
        return flask.redirect("/cassacare")

    imgName = secure_filename(f.filename)
    joinPath = os.path.join("static/images/input", imgName)
    f.save(joinPath)

    # disease = "Healthy" # default
    # information = "Your cassava is healthy"
    disease = ""
    information = ""
    details = None;
    isVideo = False;

    if imgName.endswith('.png') or imgName.endswith('.jpg') or imgName.endswith('.PNG') or imgName.endswith('.JPG'):
        label = predict("static/images/input", imgName)
    elif imgName.endswith('.mp4'):
        isVideo = True;
        dir = os.path.join("static/images/input", imgName)
        path = "static/images/input/data" + imgName
        convert(dir, imgName)
        result_futhers = []
        with ThreadPoolExecutor(max_workers=8) as executor:
            for filename in os.listdir(path):
                result_futhers.append(executor.submit(predict, path, filename))
        labels = []
        for result_futher in result_futhers:
            result = result_futher.result()
            labels.append(result)
        label = statistics.mode(labels)


        # print(label)
        # print(labels.count(label) / len(labels))
    # return str(label)
    if label == 0:
        disease = "Cassava Bacterial Blight (CBB) [0]"
        information = "Xanthomonas axonopodis pv. manihotis is the pathogen that causes bacterial blight of cassava" \
                      ". Originally discovered in Brazil in 1912, the disease has followed cultivation of cassava across the world" \
                      ". Among diseases which afflict cassava worldwide, bacterial blight causes the largest losses in terms of yield."
        details = data.cbb
        print(disease)
    elif label == 1:
        disease = "Cassava Brown Streak Disease (CBSD) [1]"
        information = "CBSD is a damaging disease of cassava plants, and is especially troublesome in East Africa." \
                      " It was found that two distinct viruses are responsible for the disease: " \
                      "cassava brown streak virus (CBSV) and Ugandan cassava brown streak virus (UCBSV)." \
                      " This disease is considered to be the biggest threat to food security in coastal East Africa and around the eastern lakes."
        details = data.cbsd
        print(disease)
    elif label == 2:
        disease = "Cassava Green Mottle (CGM) [2]"
        information = "Cassava green mottle nepovirus. It has not been confirmed to be a nepovirus; " \
                      "these are viruses that are transmitted by nematodes."
        details = data.cgm
        print(disease)
    elif label == 3:
        disease = "Cassava Mosaic Disease (CMD) [3]"
        information = "CMD is caused by viruses belonging to the genus Begomovirus in the family Geminiviridae." \
                      " It is the single most important viral disease of cassava in Zambia and it also occurs in many neighbouring countries," \
                      " including Tanzania, Malawi, Democratic Republic of the Congo, Zimbabwe, Mozambique and Angola."

        details = data.cmd
        print(disease)
    elif label == 4:
        disease = "Healthy [4]"
        information = "Your cassava is healthy"
        details = data.healthy
        print(disease)

    inputPath = 'images/input/' + imgName;

    return render_template("result/index.html", disease=disease,
                           information=information, details=details,
                           isVideo=isVideo, label=label,
                           inputPath=inputPath)

@app.route('/upload', methods=['POST'])
def my_form_post():
  if request.method == 'POST':
    f = request.files['file']
    imgName =  secure_filename(f.filename)
    f.save(os.path.join("images", imgName))
    if imgName.endswith('.png') or imgName.endswith('.jpg') or imgName.endswith('.PNG') or  imgName.endswith('.JPG'):
      label = predict("images", imgName)
    elif imgName.endswith('.mp4'):
        dir = os.path.join("images", imgName)
        convert(dir,imgName)
        path = "images/data" + imgName
        result_futhers = []
        with ThreadPoolExecutor(max_workers=8) as executor:
          for filename in os.listdir(path):
            result_futhers.append(executor.submit(predict, path, filename))
        labels = []
        for result_futher in result_futhers:
          result = result_futher.result()
          labels.append(result)
        label = statistics.mode(labels)
        print(label)
        print(labels.count(label)/len(labels))
    return str(label)



if __name__ == "__main__":
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host="0.0.0.0", port=8080, debug=True)

