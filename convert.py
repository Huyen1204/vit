# Importing all necessary libraries
import cv2
import os

def convert(path, videoName):
    # Read the video from specified path
    cam = cv2.VideoCapture(path)
    try:
        # creating a folder named data
        if not os.path.exists('static/images/input/data' + videoName):
            os.makedirs('static/images/input/data' + videoName)
    
    # if not created then raise error
    except OSError:
        print ('Error: Creating directory of data')
    # frame
    currentframe = 0
    count = 0
    
    while(True):
        # reading from frame
        ret,frame = cam.read()
    
        if ret:
            # if video is still left continue creating images
            name = './static/images/input/data' + videoName + '/frame' + str(currentframe) + '.jpg'
            if count%30 == 0 :
                cv2.imwrite(name, frame)
                print ('Creating...' + name)
                currentframe += 1
            count+=1
        else:
            break
    
    # Release all space and windows once done
    cam.release()
    cv2.destroyAllWindows()
